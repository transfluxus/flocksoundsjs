let shapes = []
let flock = new Flock()

let max_boids = 40
let numShapes = 7


let flockLayer
let lineLayer

let tones = []
let colors = []

let mouseVector = null
let shapeSelected = false

let looping = true
let mute = false

function setup(){
    createCanvas(900,700)
    flockLayer = createGraphics(width, height)
    lineLayer = createGraphics(width, height)
    lineLayer.blendMode(OVERLAY)
    rectMode(CENTER)
    ellipseMode(CENTER)
    addInitialBoid()

    tones = createToneScale('a','major',3,1)
    colors = createColorScale(tones)
    createFlockGui()

    mouseVector = createVector(0,0)
    randomSounds(7)
}

function draw(){
    // start = new Date().getTime()
    background(0)
    mouseVector.set(mouseX,mouseY,0)
    image(lineLayer,0,0)
    image(flockLayer,0,0)

    flockLayer.fill(0,50)
    flockLayer.rect(0,0,width,height)

    lineLayer.fill(0,20)
    lineLayer.rect(0,0,width,height)
    boidInit()
    flockShapeRelations()
    drawShapes()
    // flock.boids[0].position.set(mouseX,mouseY,0)
    flock.update()

    checkMouseInteraction()
    // print("t:",new Date().getTime() - start)
    // print(frameRate())
}

function boidInit(){
    if(flock.size() < max_boids && (frameCount) % 3 === 0){
        addInitialBoid()
    }
}

function drawShapes() {
    noStroke()
    for(let shape of shapes){
        shape.draw()
    }
}

function mousePressed(){
    if(mouseButton === RIGHT){
        randomShape(mouseVector.copy())
    }
}

function addInitialBoid(){
    let b = new Boid(width/2, height/2)
    flock.add(b)
}

function flockShapeRelations(){
    // reset
    for(let shape of shapes){
        shape.boidReset()
    }


    // calculate closest soundshape
    for(let b of flock.boids){
        b.calcShapeDistances()
    }
    // set shape size
    for(let shape of shapes){
        shape.normalizeSize(flock.size())
    }
}



function randomShape(pos){
    if(shapeSelected){
        return
    }
    let shapeType = random([Circle,Triangle,Square,Pulse])
    // let shapeType = random([Pulse])
    shapes.push(new shapeType(pos))
}



function keyPressed(){
    if(key === ' '){
        looping= !looping
        if(looping){
            noLoop()
        } else {
            loop()
        }
    } else if(key ==='M'){
        mute = !mute
        if(mute){
            masterVolume(0)
        } else {
            masterVolume(1)
        }
    }

}

function checkMouseInteraction(){
    shapeSelected = false
    for(let shape of shapes){
        // if we have something unselect the rest
        if(shapeSelected){
            shape.select(false)
            continue
        }
        if(p5.Vector.sub(shape.position,mouseVector).magSq() < 400) {
            shape.select(true)
            shapeSelected = true
        } else {
            shape.select(false)
        }
    }
}

function  randomSounds(num) {
    for(let i =0; i < num; i++){
        let pos = createVector(random(width),random(height))
        randomShape(pos)
    }
}