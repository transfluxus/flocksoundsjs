
// let tbase = teoria.note('a')

function createToneScale(baseTone, scale, octave, octaves){
    let tBaseTone = teoria.note(baseTone)
    // print(tBaseTone.fq())
    let scaleTones = tBaseTone.scale(scale).simple()
    let tScaleTones = []
    // print(scaleTones)
    for(let i  in scaleTones){
        // print(i)
        let tNote = teoria.note(scaleTones[i])
        // print(tNote)
        tNote.transpose(tBaseTone)
        tScaleTones.push(tNote.fq())
        // print(tScaleTones[i].name(), tScaleTones[i].octave(), tScaleTones[i].fq())
    }
    return tScaleTones
}

function createColorScale(tones){
    let min = Math.min(...tones)
    let max = Math.max(...tones)

    hues = tones.map(fq => map(fq,min,max,0,255))
    colorMode(HSB)
    colors = hues.map(hue => color(hue,255,255))
    colorMode(RGB)
    return colors
}