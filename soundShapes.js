
minShapeSize = 10
maxShapeSize = 70

function* nextShapeId (){
    let nextShapeId = 0
    while(true){
        yield nextShapeId++
    }
}

nextIdGen = nextShapeId()




function getMirrorIndexToTranslate(index){
    // print(index,(index % 3) - 1, Math.round(index / 3) - 1)
    return getMirrorTranslate((index % 3) - 1, Math.round(index / 3) - 1)
}

function getMirrorTranslate(x, y){
    return createVector(x * width,y * height)
}


class SoundShape {

    constructor(position, soundType){
        this.position = position
        this.mirroredLocations = this.getMirroredLocations()
        // print(this.mirroredLocations.length)
        this.torque = 0
        this.scale = 0
        this.size = minShapeSize

        this.boidCount = 0
        // this.boidCenter = createVector(0,0)
        this.id = nextIdGen.next().value
        // this.hasBoids = false
        this.scaleIndex = random(0,tones.length) | 0
        this.color = colors[this.scaleIndex]
        this.sound = new p5.Oscillator(tones[this.scaleIndex],soundType)
        this.sound.add(0)
        this.sound.start()
        this.orientation = random(TAU)
        this.selected = false
    }

    getMirroredLocations() {
        let mirroredLocations = []
        for(let y = -1; y <= 1; y++) {
            for(let x = -1; x <= 1; x++) {
                if(x === 0 && y === 0) {
                    continue
                }
                mirroredLocations.push(this.position.copy().add(getMirrorTranslate(x,y)))
            }
        }
        return mirroredLocations
    }



    normalizeSize(max_boids){
        this.scale = this.boidCount  / max_boids
        //print(this.boidCount)
        if(this.boidCount > 0) {
            // this.boidCenter.div(this.boidCount)
            this.hasBoids = true
        }
    }

    boidReset(){
        this.boidCount = 0
        this.hasBoids = false
        // this.boidCenter.set(this.position.x, this.position.y,0)
    }

    addBoid(b){
        this.boidCount += 1
        // this.boidCenter.add(b.position)
        this.torque += this.getTorque(b)
    }

    getTorque(b){
        let momentArm = p5.Vector.sub(b.position,this.position)
        let parallelComponent = momentArm.mult(p5.Vector.dot(b.velocity, momentArm) / (p5.Vector.dot(momentArm, momentArm)))
        let angularForce = p5.Vector.sub(b.velocity,parallelComponent)
        return angularForce.cross(b.position)
        // let torque = angularForce.mult(momentArm.mag())
        // return torque.y * 0.001
    }


    pre(){
        push()
        translate(this.position.x, this.position.y)
        this.orientation += this.torque
        this.torque *= 0.8
        rotate(this.orientation)
        if(!this.selected){
            fill(this.color)
        } else {
            noFill()
            strokeWeight(3)
            stroke(this.color)
        }
        this.size = map(this.scale,0,1,minShapeSize,maxShapeSize)
    }

    post(){
        pop()
        this.sound.amp(this.scale)
    }

    select(selected){
        this.selected = selected
    }
}


class Circle extends SoundShape {

    constructor(position){
        super(position, "sine")
    }


    draw(){
        this.pre()
        ellipse(0,0, this.size, this.size)
        this.post()
    }

}


class Triangle extends SoundShape {

    constructor(position){
        super(position, "triangle")
        this.v1 = p5.Vector.fromAngle(0)
        this.v2 = p5.Vector.fromAngle(TAU / 3)
        this.v3 = p5.Vector.fromAngle(2 * TAU / 3)
    }


    draw(){
        this.pre()
        beginShape()
        this.v1.setMag(this.size)
        this.v2.setMag(this.size)
        this.v3.setMag(this.size)
        triangle(this.v1.x,this.v1.y,this.v2.x,this.v2.y,this.v3.x,this.v3.y)
        this.post()
    }
}

class Square extends SoundShape {

    constructor(position){
        super(position, "square")
    }

    draw(){
        this.pre()
        rect(0,0, this.size, this.size)
        this.post()
    }

}

class Pulse extends SoundShape {

    constructor(position){
        super(position, "sawtooth")
    }

    draw(){
        this.pre()
        stroke(this.color)
        if(!this.selected){
            strokeWeight(5)
            line(-this.size,0,this.size,0)
        } else {
            strokeWeight(2)
            line(-this.size,-3,this.size,-3)
            line(-this.size,3,this.size,3)
        }
        this.post()
    }

}