
var gui

function createFlockGui(){
    gui = createGui('Flock',width,20)

    sliderRange(0.5, 5, 0.25)
    gui.addGlobals('maxSpeed')

    sliderRange(0.01, 0.5, 0.01)
    gui.addGlobals('maxForce')

    sliderRange(0, 2, 0.25)
    gui.addGlobals('separate','align')
    sliderRange(0,1,0.2)
    gui.addGlobals('cohesion')

}
