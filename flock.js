var maxSpeed = 3
var maxForce = 0.05

var separate = 1.5
var align = 1
var cohesion = 1

class Flock {

    constructor(){
        this.boids = []
    }

    update(){
        for(let boid of this.boids){
            boid.run(this.boids)
        }
    }

    add(boid){
        this.boids.push(boid);
    }

    size(){
        return this.boids.length
    }
}

boidId = 0

class Boid {

    constructor(x, y) {
        this.acceleration = createVector(0, 0);
        this.velocity = createVector(random(-1, 1), random(-1, 1));
        this.position = createVector(x, y);
        this.r = 10.0;
        this.shapeDistances = null
        this.id = boidId
        boidId ++
        // this.maxforce = maxForce // Maximum steering force
    }

    run(boids) {
        this.flock(boids);
        this.update();
        this.borders();
        this.render();
    }

    flock(boids) {
        let sep = this.separate(boids);   // Separation
        let ali = this.align(boids);      // Alignment
        let coh = this.cohesion(boids);   // Cohesion
        // Arbitrarily weight these forces
        sep.mult(separate);
        ali.mult(align);
        coh.mult(cohesion);
        // Add the force vectors to acceleration
        this.applyForce(sep);
        this.applyForce(ali);
        this.applyForce(coh);
    }

// Method to update location
    update() {
        // Update velocity
        this.velocity.add(this.acceleration);
        // Limit speed
        this.velocity.limit(maxSpeed);
        this.position.add(this.velocity);
        // Reset accelertion to 0 each cycle
        this.acceleration.mult(0);
    }

// A method that calculates and applies a steering force towards a target
// STEER = DESIRED MINUS VELOCITY
    seek(target) {
        let desired = p5.Vector.sub(target,this.position);  // A vector pointing from the location to the target
        // Normalize desired and scale to maximum speed
        desired.normalize();
        desired.mult(maxSpeed);
        // Steering = Desired minus Velocity
        let steer = p5.Vector.sub(desired,this.velocity);
        steer.limit(maxForce);  // Limit to maximum steering force
        return steer;
    }

   render() {
        // Draw a triangle rotated in the direction of velocity

       let theta = this.velocity.heading() + radians(90);
        //flockLayer.noFill()

        let dist_sum = this.shapeDistances[0].dist + this.shapeDistances[1].dist
        let dist_1_ratio = this.shapeDistances[0].dist / dist_sum
        let colorInterpolate = lerpColor(this.shapeDistances[0].s.color, this.shapeDistances[1].s.color, dist_1_ratio)

        let boidColor = d3.scaleOrdinal().domain([
            this.shapeDistances[0].s.id,
            this.shapeDistances[1].s.id,
            this.shapeDistances[2].s.id
        ]).range([
            this.shapeDistances[0].s.color,
            this.shapeDistances[1].s.color,
            this.shapeDistances[2].s.color
        ])
        if(this.id === 1)
        console.log(boidColor());
        //flockLayer.fill(boidColor());
        
        flockLayer.stroke(200);
        flockLayer.push();
        flockLayer.scale(1 / window.devicePixelRatio)
        flockLayer.translate(this.position.x,this.position.y);
        flockLayer.rotate(theta);
        flockLayer.beginShape();
        flockLayer.vertex(0, -this.r*2);
        flockLayer.vertex(-this.r, this.r*2);
        flockLayer.vertex(this.r, this.r*2);
        flockLayer.endShape(CLOSE);
        flockLayer.pop();
     //  this.render2()
    }

    render2() {
        // Draw a triangle rotated in the direction of velocity
        let theta = this.velocity.heading() + radians(90);
        noFill()
        // fill(127, 30);
        stroke(200);
        push();
        translate(this.position.x,this.position.y);
        rotate(theta);
        beginShape();
        vertex(0, -this.r*2);
        vertex(-this.r, this.r*2);
        vertex(this.r, this.r*2);
        endShape(CLOSE);
        pop();
    }

// Wraparound
    borders() {
        if (this.position.x < -this.r)  this.position.x = width +this.r;
        if (this.position.y < -this.r)  this.position.y = height+this.r;
        if (this.position.x > width +this.r) this.position.x = -this.r;
        if (this.position.y > height+this.r) this.position.y = -this.r;
    }

// Separation
// Method checks for nearby boids and steers away
    separate(boids) {
        let desiredseparation = 25.0;
        let steer = createVector(0,0);
        let count = 0;
        // For every boid in the system, check if it's too close
        for (let i = 0; i < boids.length; i++) {
            let d = p5.Vector.dist(this.position,boids[i].position);
            // If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
            if ((d > 0) && (d < desiredseparation)) {
                // Calculate vector pointing away from neighbor
                let diff = p5.Vector.sub(this.position,boids[i].position);
                diff.normalize();
                diff.div(d);        // Weight by distance
                steer.add(diff);
                count++;            // Keep track of how many
            }
        }
        // Average -- divide by how many
        if (count > 0) {
            steer.div(count);
        }

        // As long as the vector is greater than 0
        if (steer.mag() > 0) {
            // Implement Reynolds: Steering = Desired - Velocity
            steer.normalize();
            steer.mult(maxSpeed);
            steer.sub(this.velocity);
            steer.limit(maxForce);
        }
        return steer;
    }

// Alignment
// For every nearby boid in the system, calculate the average velocity
    align(boids) {
        let neighbordist = 50;
        let sum = createVector(0,0);
        let count = 0;
        for (let i = 0; i < boids.length; i++) {
            let d = p5.Vector.dist(this.position,boids[i].position);
            if ((d > 0) && (d < neighbordist)) {
                sum.add(boids[i].velocity);
                count++;
            }
        }
        if (count > 0) {
            sum.div(count);
            sum.normalize();
            sum.mult(maxSpeed);
            let steer = p5.Vector.sub(sum,this.velocity);
            steer.limit(maxForce);
            return steer;
        } else {
            return createVector(0,0);
        }
    }

// Cohesion
// For the average location (i.e. center) of all nearby boids, calculate steering vector towards that location
    cohesion(boids) {
        let neighbordist = 50;
        let sum = createVector(0,0);   // Start with empty vector to accumulate all locations
        let count = 0;
        for (let i = 0; i < boids.length; i++) {
            let d = p5.Vector.dist(this.position,boids[i].position);
            if ((d > 0) && (d < neighbordist)) {
                sum.add(boids[i].position); // Add location
                count++;
            }
        }
        if (count > 0) {
            sum.div(count);
            return this.seek(sum);  // Steer towards the location
        } else {
            return createVector(0,0);
        }
    }


    applyForce(force) {
        // We could add mass here if we want A = F / M
        this.acceleration.add(force);
    }


    calcShapeDistances() {
        // get more then the best
        let closest_dist = Infinity
        let closest_shape = null
        let mirrorIndex = -1

        let distances = Array()


        for(let shape of shapes){
            let distSq = p5.Vector.sub(this.position, shape.position).magSq()
            let closest_from_shape = distSq

            if(distSq < closest_dist){
                closest_dist = distSq
                closest_shape = shape
                mirrorIndex = -1
            }
            for(let i=0; i < shape.mirroredLocations.length; i++){
                let mirror = shape.mirroredLocations[i]
                let distSq = p5.Vector.sub(this.position, mirror).magSq()
                if(distSq < closest_from_shape) {
                    closest_from_shape = distSq
                }
                if(distSq < closest_dist){
                    closest_dist = distSq
                    closest_shape = shape
                    mirrorIndex = i
                }
            }
            distances.push( { dist: closest_from_shape, s: shape})
        }
        // console.log(distances)
        this.shapeDistances = distances.sort(function(x, y){
        return d3.ascending(x.dist,y.dist)}
        );
        //console.log(sorted_shapes)
        //console.log(this.shapeDistances[0].s.id, this.shapeDistances[1].s.id);

        if(closest_shape !== null){
            closest_shape.addBoid(this)
            // draw connection
            let re = red(closest_shape.color)
            let gr = green(closest_shape.color)
            let bl = blue(closest_shape.color)
            stroke(re,gr,bl,100)
            if(mirrorIndex === -1) {
                line(this.position.x,this.position.y, closest_shape.position.x, closest_shape.position.y)
            } else{
                lineLayer.line(this.position.x,this.position.y,
                    closest_shape.mirroredLocations[mirrorIndex].x,
                    closest_shape.mirroredLocations[mirrorIndex].y)
                lineLayer.line(closest_shape.position.x,closest_shape.position.y,
                    this.position.x + (closest_shape.position.x - closest_shape.mirroredLocations[mirrorIndex].x),
                    this.position.y + (closest_shape.position.y - closest_shape.mirroredLocations[mirrorIndex].y))
            }
        }
    }
}

